# LogMe

LogMe is MS Word addin that keeps a record for everything you do while using word.

After running the setup LogMe can been found in the FILE tab of Microsoft word

![image](https://user-images.githubusercontent.com/94911727/156038642-286f37d3-07ee-4dad-8ecc-576c5780ec40.png)


![image](https://user-images.githubusercontent.com/94911727/156038684-01174312-c105-42f9-b648-2795be7ab4f9.png)

When you click on the Open LogMe button you should be able to see your log files.

The Log file contains information like the date and the time about all the actions you did while using word.

Here is an example 

![image](https://user-images.githubusercontent.com/94911727/156038759-1741fa87-2f31-4c4b-b7b1-babfe9706308.png)
